package com.SDM.tutoria;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Calendar;

import android.app.Activity;
import android.content.ContentValues;
//import com.mobsandgeeks.saripaar.*;
import android.content.Intent;
//import net.sqlcipher.database.SQLiteDatabase;
//import net.sqlcipher.database.SQLiteDatabase;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class ShowProfile extends Activity {

	ContactsPersistence helper;
	SQLiteDatabase database;

	static Cursor result_set;

	EditText nameField, dateField, emailField, notesField;

	Button back, home, edit;

	String email_addr;
	String[] content_id = new String[1];

	/*
	 * EDIT setcontent so to decrypt the retrieved data
	 * Edit on create method to encrypt data
	 * (non-Javadoc)
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */

	protected void onCreate(Bundle savedInstanceState) {

		helper = new ContactsPersistence(getApplicationContext(), "contactsDB", null, 1);
		database = helper.getWritableDatabase();


		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_show_profile);
		home = (Button)findViewById(R.id.pgoto_home);
		home.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(ShowProfile.this, Main.class);
				startActivity(intent);
			}
		});

		back = (Button)findViewById(R.id.pgoto_back);
		back.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(ShowProfile.this, ShowBirthday.class);
				startActivity(intent);
			}
		});

		//		edit = (Button)findViewById(R.id.pgoto_back);
		//		edit.setOnClickListener(new View.OnClickListener() {
		//			
		//			@Override
		//			public void onClick(View v) {
		//				Intent intent = new Intent(ShowProfile.this, EditProfile.class);
		//				startActivity(intent);
		//			}
		//		});

		get_contact();
		int tmp = result_set.getCount();
		set_content();

		edit = (Button)findViewById(R.id.edit);
		edit.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				int total = 0;
				boolean accepted_date  = false;
				boolean accepted_email = false;
				boolean empty_mail     = false;
				boolean empty_date     = false;

				nameField = (EditText) findViewById(R.id.name);
				String name = nameField.getText().toString();
				Log.i("[debug]", "the content of name is_" + name + "_ __");
				if(TextUtils.isEmpty(name)){
					Log.w("[debug]", "name is empty");
					nameField.setError(getString(R.string.required_field));
				}
				dateField = (EditText) findViewById(R.id.birthdate);
				String date = dateField.getText().toString();
				if(TextUtils.isEmpty(date)){
					Log.w("[debug]", "date is empty");
					empty_date = true;
					dateField.setError(getString(R.string.required_field));
				}else{
					/*
					 * Date validation
					 */
					String [] date_format = date.split("-", 3);

					for(int i = 0; i < date_format.length; i++){
						switch(i){
						case 0: 
							if(Integer.parseInt(date_format[i]) <= 31) {
								total++;
								Log.d("[debug]", "day is " + date_format[i]);
							}
							break;
						case 1: 
							if(Integer.parseInt(date_format[i]) <= 12){
								total++;
								Log.d("[debug]", "month is " + date_format[i]);
							}
							break;
						case 2:
							if(Integer.parseInt(date_format[i]) <= Calendar.getInstance().get(Calendar.YEAR)){
								total++;
								Log.d("[debug]", "year is " + date_format[i] + " and this year is " + Calendar.getInstance().get(Calendar.YEAR));
							}
							break;
						default: Log.i("[DEBUG]", "Value of i: " + i);
						}
					}
					if(total == 3) accepted_date = true;
					Log.i("[debug]", "Total is " + total);
				}

				emailField = (EditText) findViewById(R.id.email);
				String email = emailField.getText().toString();
				if(TextUtils.isEmpty(email)){
					Log.w("[debug]", "email is empty");
					empty_mail = true;
					emailField.setError(getString(R.string.required_field));
				}else{
					/*
					 * Email validation
					 */
					for(int i = 0; i < email.length(); i++){
						if(email.charAt(i) == '@'){
							accepted_email = true;
						}
					}
				}								

				notesField = (EditText) findViewById(R.id.notes);
				String notes = notesField.getText().toString();
				/*
				 * Check validation
				 */
				if(accepted_date && accepted_email){
					Log.i("[debug]", "accepted record, inserting...");

					ContentValues values = new ContentValues();
					try {
						values.put(ContactsPersistence.CONTACTS_NAME, EncryptionTools.encrpytWithPBE(Base64.encode(name.getBytes(),0),  Login.password, Main.iv_iv, Main.salt, Main.secretKey));
						values.put(ContactsPersistence.CONTACTS_DATE, EncryptionTools.encrpytWithPBE(Base64.encode(date.getBytes(),0),  Login.password, Main.iv_iv, Main.salt, Main.secretKey));
						values.put(ContactsPersistence.CONTACTS_EMAIL, EncryptionTools.encrpytWithPBE(Base64.encode(email.getBytes(),0),  Login.password, Main.iv_iv, Main.salt, Main.secretKey));
						values.put(ContactsPersistence.CONTACTS_NOTES, EncryptionTools.encrpytWithPBE(Base64.encode(notes.getBytes(),0),  Login.password, Main.iv_iv, Main.salt, Main.secretKey));
					} catch (GeneralSecurityException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					result_set.moveToFirst();
					long id = database.update(ContactsPersistence.CONTACTS_TABLE, values,  "id=?", content_id);

					//					database.execSQL("update contacts set name='shehab' where email='manu@gmail.com';");


					//					if(id != -1){
					//						Log.d("[debug]", "Contact created " + id);
					//					}


					/* Other, less safe way:
					 * String query="INSERT INTO contacts(name, birthday, email, notes) VALUES ('"+name+"', '"+date+"', '"+email+"', '"+notes+"');";
					 * database.execSQL(query);
					 * 
					 */
					database.close();
					Toast.makeText(ShowProfile.this, "Birthday successfully updated", Toast.LENGTH_SHORT).show();
					//					if (nameField != null) nameField.setText("");
					//					if (dateField != null) dateField.setText("");
					//					if (emailField != null) emailField.setText("");
					//					if (notesField != null) notesField.setText("");
					nameField.requestFocus();

				}else if(!accepted_date && !empty_date){
					dateField.setError(getString(R.string.required_field));
					dateField.setError("Introduce date in the correct format [dd-mm-yyyy]");
					if (dateField != null) dateField.setText("");
					dateField.requestFocus();
					//Toast.makeText(AddBirthday.this, "Introduce date in the correct format [dd-mm-yyyy]", Toast.LENGTH_SHORT).show();
					if(!accepted_email && !empty_mail){
						//Toast.makeText(AddBirthday.this, "Introduce email in the correct format [aaaa@bbbb.c]", Toast.LENGTH_SHORT).show();
						emailField.setError("Introduce email in the correct format [a@b.c]");
						if (emailField != null) emailField.setText("");
					}
				}else if(!accepted_email && !empty_mail){
					Toast.makeText(ShowProfile.this, "Introduce email in the correct format [aaaa@bbbb.c]", Toast.LENGTH_SHORT).show();
					if (emailField != null) emailField.setText("");
					emailField.requestFocus();
				}



				//Necessary?
				accepted_email = false;
				accepted_date = false;
				total = 0;

			}
		});
	}



	private void get_contact() {

		helper = new ContactsPersistence(getApplicationContext(), "contactsDB", null, 1);
		database=helper.getReadableDatabase();

		String [] columns = new String [5];
		columns[0] = "id";
		columns[1] = "name";
		columns[2] = "birthday";
		columns[3] = "email";
		columns[4] = "notes";

		Intent my_intent = getIntent();
		content_id[0] = my_intent.getStringExtra("contact_id");

		result_set = database.query("contacts", columns, "id = ?", content_id, null, null, null);
		//		database.close();

	}

	private void set_content() {
		
		int tmp = result_set.getCount();
		result_set.moveToFirst();
		
		nameField = (EditText)findViewById(R.id.name);
		dateField = (EditText)findViewById(R.id.birthdate);
		emailField = (EditText)findViewById(R.id.email);
		notesField = (EditText)findViewById(R.id.notes);

		String aux_name = null;
		String aux_date = null;
		String aux_email = null;
		String aux_notes = null;
		
		byte[] aux_name_arr = null;
		byte[] aux_date_arr = null;
		byte[] aux_email_arr = null;
		byte[] aux_notes_arr = null;
		
		try {
			aux_name_arr  = EncryptionTools.decrpytWithPBE(result_set.getBlob(1), Login.password, Main.iv_iv, Main.salt, Main.secretKey);
			aux_date_arr  = EncryptionTools.decrpytWithPBE(result_set.getBlob(2), Login.password, Main.iv_iv, Main.salt, Main.secretKey);
			aux_email_arr = EncryptionTools.decrpytWithPBE(result_set.getBlob(3), Login.password, Main.iv_iv, Main.salt, Main.secretKey);
			aux_notes_arr = EncryptionTools.decrpytWithPBE(result_set.getBlob(4), Login.password, Main.iv_iv, Main.salt, Main.secretKey);
			aux_name  = new String(Base64.decode(aux_name_arr, 0));
			aux_date  = new String(Base64.decode(aux_date_arr, 0));
			aux_email = new String(Base64.decode(aux_email_arr, 0));
			aux_notes = new String(Base64.decode(aux_notes_arr, 0));
		} catch (GeneralSecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		nameField.setText(aux_name);
		dateField.setText(aux_date);
		emailField.setText(aux_email);
		notesField.setText(aux_notes);

	}
}
