package com.SDM.tutoria;

import com.SDM.tutoria.R;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

public class SongService extends Service {
	boolean play = true;
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
	
	MediaPlayer mySong;
	@Override
	public int onStartCommand(Intent intent, int flags, int startId){
		Log.i("Service", "In method onStartCommand, service started!");
		if(play){
			mySong = MediaPlayer.create(this, R.raw.happy_birthday); 				
			//mySong.setAudioStreamType(AudioManager.STREAM_MUSIC);
			mySong.start();
			play = false;
			Toast.makeText(this, "The service has started", Toast.LENGTH_SHORT).show();
		}
		return START_STICKY;
	}
	
	
	@Override
	public void onDestroy(){
		if(!play){
			super.onDestroy();	
			mySong.stop();	
			Log.i("Service", "In method onDestroy, service finished!");
			Toast.makeText(this, "Service stopped", Toast.LENGTH_SHORT).show();
			play = true;
		}
	}

}
