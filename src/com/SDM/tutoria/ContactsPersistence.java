package com.SDM.tutoria;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.spec.IvParameterSpec;

import android.content.ContentValues;
import android.content.Context;
//import net.sqlcipher.database.SQLiteDatabase;
//import net.sqlcipher.database.SQLiteOpenHelper;
//import net.sqlcipher.database.SQLiteDatabase.CursorFactory;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

public class ContactsPersistence extends SQLiteOpenHelper {
	public static final String CONTACTS_TABLE        = "contacts";
	public static final String CONTACTS_ID			 = "id";
	public static final String CONTACTS_NAME         = "name";
	public static final String CONTACTS_DATE         = "birthday";
	public static final String CONTACTS_EMAIL      	 = "email";
	public static final String CONTACTS_NOTES        = "notes";

	public static final String LOGIN_TABLE           = "login";
	public static final String LOGIN_NAME            = "username";
	public static final String LOGIN_PASSWORD        = "password";
	public static final String IV                    = "iv";
	public static final String SALT                  = "salt";

	String create_contacts = "CREATE TABLE " + CONTACTS_TABLE + "(" +
			CONTACTS_ID	   + " INTEGER PRIMARY KEY AUTOINCREMENT," +
			CONTACTS_NAME  + " BLOB NOT NULL," +
			CONTACTS_DATE  + " BLOB NOT NULL," +
			CONTACTS_EMAIL + " BLOB NOT NULL," +
			CONTACTS_NOTES + " BLOB);";

	String create_login = "CREATE TABLE " + LOGIN_TABLE + "(" +
			LOGIN_NAME      + " TEXT PRIMARY KEY NOT NULL," +
			LOGIN_PASSWORD  + " TEXT NOT NULL," + 
			IV              + " BLOB NOT NULL," + 
			SALT            + " BLOB NOT NULL);";
	
	EncryptionTools encrypter = new EncryptionTools();

	String insert_login = "INSERT INTO " + LOGIN_TABLE + "(" + LOGIN_NAME + ", " + LOGIN_PASSWORD + ", " 
						+ IV + ", " + SALT + ") VALUES (user, 1234, " + EncryptionTools.generateRandomByteArray(32) 
						+ ", " + EncryptionTools.generateRandomByteArray(32) + ");";

	private String pSHAHash, uSHAHash;

	public ContactsPersistence(Context context, String name, CursorFactory factory, int version) {
		super(context, name, factory, version);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(create_contacts);
		db.execSQL(create_login);

		String user     = "user";
		String password = "1234";
		byte[] iv       = EncryptionTools.generateRandomByteArray(16);
		byte[] salt     = EncryptionTools.generateRandomByteArray(16);
		IvParameterSpec iv_iv = EncryptionTools.getIV(iv);
		
		password += R.string.salty_salt;

		System.out.println("creation: vamos a ver que pasa aqui: " +  password);
		MessageDigest mdSha1 = null;
		try
		{
			mdSha1 = MessageDigest.getInstance("SHA-256");
			mdSha1.update(password.toString().getBytes()); // obtenemos la secuencias de bytes
		} catch (NoSuchAlgorithmException e1) {
			Log.e("miApp", "Error al inicializar el SHA1 message digest");
		}
		byte[] pdata = mdSha1.digest(); // pasamos los digitos a un arreglo de bytes
		
		try {
			pSHAHash = convertToHex(pdata);
		} catch (IOException e2) {
			e2.printStackTrace();
		}
		//System.out.println("creation: vamos a ver que pasa y aqui tambien: " +  pSHAHash);
		
		mdSha1 = null;
		
		try
		{
			mdSha1 = MessageDigest.getInstance("SHA-256");
			mdSha1.update(user.toString().getBytes()); // obtenemos la secuencias de bytes
		} catch (NoSuchAlgorithmException e1) {
			Log.e("miApp", "Error al inicializar el SHA1 message digest");
		}
		byte[] udata = mdSha1.digest(); // pasamos los digitos a un arreglo de bytes
		
		try {
			uSHAHash = convertToHex(udata);
		} catch (IOException e) {
			e.printStackTrace();
		}
		//System.out.println("creation: vamos a ver que pasa y aqui tambien: " +  uSHAHash);
		
		ContentValues values = new ContentValues();
		
		
		values.put(ContactsPersistence.LOGIN_NAME, uSHAHash);
		values.put(ContactsPersistence.LOGIN_PASSWORD, pSHAHash);
		
		values.put(ContactsPersistence.IV, iv_iv.getIV());
		values.put(ContactsPersistence.SALT, salt);
		
		long id = db.insert(ContactsPersistence.LOGIN_TABLE, null, values);
		if(id != -1){
			Log.d("[debug]", "Contact created " + id + "iv = " + EncryptionTools.bytesToHex(iv_iv.getIV()) + "salt = " + salt);
		}
		//db.execSQL(insert_login);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS contacts");
		db.execSQL("DROP TABLE IF EXISTS login");		
		onCreate(db);
	}
	private static String convertToHex(byte[] data) throws java.io.IOException
	{         
		StringBuffer sb = new StringBuffer();
		for(byte recogerDatos : data)
		{
			sb.append(Integer.toString((recogerDatos & 0xff) + 0x100, 16).substring(1));
		}
		return sb.toString();
	}
}
