package com.SDM.tutoria;
	
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;

import android.util.Log;
/*
 * Actuation line:
 * - Modify DB tables to store salt and IV
 * - Modify DB table so to store byte[] instead of String
 * - Use EncryptionTools object to cipher and decipher user inputs
 * 
 * - Make final improvement, remove users from DB? very very simple, 0 work :) 
 */


public class EncryptionTools {
	//private static IvParameterSpec iv = null; 
	//private static byte[] salt = null;
	final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
	public static String bytesToHex(byte[] bytes) {
	    char[] hexChars = new char[bytes.length * 2];
	    for ( int j = 0; j < bytes.length; j++ ) {
	        int v = bytes[j] & 0xFF;
	        hexChars[j * 2] = hexArray[v >>> 4];
	        hexChars[j * 2 + 1] = hexArray[v & 0x0F];
	    }
	    return new String(hexChars);
	}
	
	
	public static byte[] generateRandomByteArray(int sizeInBytes) {
		byte[] randomNumberByteArray = new byte[sizeInBytes];
		// populate the array with random bytes using non seeded secure random 
		new SecureRandom().nextBytes(randomNumberByteArray);
		return randomNumberByteArray;
	}

	public static IvParameterSpec getIV(byte[]iv_iv) {	
		IvParameterSpec iv = new IvParameterSpec(iv_iv); 
		return iv;
	}

	public static byte[] getSalt() {
		byte [] salt = generateRandomByteArray(32); 
		return salt;
	}
	
	/*
	 * Use the same secret key for the whole application -> generate it only once.
	 * bytestoHex(ck1.getEncoded());
	 */
	
	
	public static SecretKey generatePBEKey(char[] password, byte[] salt) throws NoSuchAlgorithmException, InvalidKeySpecException {
		final int iterations = 10000;
		final int outputKeyLength = 256;
		SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
		KeySpec keySpec = new PBEKeySpec(password, salt, iterations, outputKeyLength); 
		SecretKey secretKey = secretKeyFactory.generateSecret(keySpec);
		return secretKey;
	}
	
	public static byte[] encrpytWithPBE(byte[] painText, String userPassword, IvParameterSpec iv_iv, byte[] m_salt, SecretKey secretKey)
			throws GeneralSecurityException, IOException {
			//SecretKey secretKey = generatePBEKey(userPassword.toCharArray(), m_salt); 
			Log.i("[Debug]","ENCRYPT Secret key is " + EncryptionTools.bytesToHex(secretKey.getEncoded()));
			final Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding"); 
			cipher.init(Cipher.ENCRYPT_MODE, secretKey, iv_iv);
			//Posible caso de fallo 1 -> ASCII
//			return cipher.doFinal(painText.getBytes("ASCII")); 
			return cipher.doFinal(painText); 

	}
	
	public static byte[] decrpytWithPBE(byte[] cipherText, String userPassword, IvParameterSpec iv_iv, byte[] m_salt, SecretKey secretKey) throws GeneralSecurityException, IOException {
		//SecretKey secretKey = generatePBEKey(userPassword.toCharArray(), m_salt); 
		Log.i("[Debug]","DECRYPT Secret key is " + EncryptionTools.bytesToHex(secretKey.getEncoded()));
		final Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding"); 
		cipher.init(Cipher.DECRYPT_MODE, secretKey, iv_iv);
		
		return cipher.doFinal(cipherText);
	}
	
}
