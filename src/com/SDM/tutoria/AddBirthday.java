package com.SDM.tutoria;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Calendar;

import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;

import android.app.Activity;
import android.content.ContentValues;
//import com.mobsandgeeks.saripaar.*;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class AddBirthday extends Activity {

	ContactsPersistence helper;
	SQLiteDatabase database;
	Button home, show, add;
	Cursor result_set;
	EncryptionTools encrypter;
	//public static byte[] iv;
	//public static byte[] salt;
	//public static SecretKey secretKey;
	byte[] date_ciphered, email_ciphered, name_ciphered, notes_ciphered;
	//public static IvParameterSpec iv_iv; 
	byte[] date_b64;
	byte[] email_b64;
	byte[] name_b64;
	byte[] notes_b64;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_birthday);

		helper = new ContactsPersistence(getApplicationContext(), "contactsDB", null, 1);
		database = helper.getWritableDatabase();

		show = (Button)findViewById(R.id.goto_view);
		show.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String [] columns = new String [4];
				columns[0] = "name";
				columns[1] = "birthday";
				//Future development: Use these values
				columns[2] = "email";
				columns[3] = "notes";
				final Cursor result_set = database.query("contacts", columns, null, null, null, null, null);
				if(result_set.getCount() > 0){				
					Intent intent = new Intent(AddBirthday.this, ShowBirthday.class);
					startActivity(intent);
				} else{
					Toast.makeText(AddBirthday.this, "You do not have any reminders", Toast.LENGTH_LONG).show();
				}
			}
		});

		home = (Button)findViewById(R.id.goto_home);
		home.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(AddBirthday.this, Main.class);
				startActivity(intent);
			}
		});

		add = (Button)findViewById(R.id.add_bday);
		add.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				int total = 0;
				boolean accepted_date  = false;
				boolean accepted_email = false;
				boolean empty_mail     = false;
				boolean empty_date     = false;

				final EditText nameField = (EditText) findViewById(R.id.et_name);
				String name = nameField.getText().toString();
				Log.i("[debug]", "the content of name is_" + name + "_ __");
				if(TextUtils.isEmpty(name)){
					Log.w("[debug]", "name is empty");
					nameField.setError(getString(R.string.required_field));
				}
				final EditText dateField = (EditText) findViewById(R.id.et_date);
				String date = dateField.getText().toString();
				if(TextUtils.isEmpty(date)){
					Log.w("[debug]", "date is empty");
					empty_date = true;
					dateField.setError(getString(R.string.required_field));
				}else{
					/*
					 * Date validation
					 */
					String [] date_format = date.split("-", 3);

					for(int i = 0; i < date_format.length; i++){
						switch(i){
						case 0: 
							if(Integer.parseInt(date_format[i]) <= 31) {
								total++;
								Log.d("[debug]", "day is " + date_format[i]);
							}
							break;
						case 1: 
							if(Integer.parseInt(date_format[i]) <= 12){
								total++;
								Log.d("[debug]", "month is " + date_format[i]);
							}
							break;
						case 2:
							if(Integer.parseInt(date_format[i]) <= Calendar.getInstance().get(Calendar.YEAR)){
								total++;
								Log.d("[debug]", "year is " + date_format[i] + " and this year is " + Calendar.getInstance().get(Calendar.YEAR));
							}
							break;
						default: Log.i("[DEBUG]", "Value of i: " + i);
						}
					}
					if(total == 3) accepted_date = true;
					Log.i("[debug]", "Total is " + total);
				}

				final EditText emailField = (EditText) findViewById(R.id.et_mail);
				String email = emailField.getText().toString();
				if(TextUtils.isEmpty(email)){
					Log.w("[debug]", "email is empty");
					empty_mail = true;
					emailField.setError(getString(R.string.required_field));
				}else{
					/*
					 * Email validation
					 */
					for(int i = 0; i < email.length(); i++){
						if(email.charAt(i) == '@'){
							accepted_email = true;
						}
					}
				}								

				final EditText notesField = (EditText) findViewById(R.id.et_notes);
				String notes = notesField.getText().toString();
				/*
				 * Check validation
				 */
				if(accepted_date && accepted_email){
					Log.i("[debug]", "accepted record, retrieving iv and salt...");
					
					/*
					 * Start of new 
					 */
					
					helper = new ContactsPersistence(getApplicationContext(), "contactsDB", null, 1);
					database=helper.getReadableDatabase();
					
					/*String [] columns = new String [2];
					columns[0] = "iv";
					columns[1] = "salt";										
					
					result_set = database.query("login", columns, null, null, null, null, null);
					//result_set = database.
					
					if(result_set != null) {
						result_set.moveToFirst();
						//iv   = result_set.getString(0).getBytes();
						iv   = result_set.getBlob(0);
						salt =  result_set.getBlob(1);
						
						iv_iv = new IvParameterSpec(iv);
					}*/
					
					/*try {
						SecretKey key = EncryptionTools.generatePBEKey(((new Login()).password).toCharArray(), salt);
					} catch (NoSuchAlgorithmException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (InvalidKeySpecException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}*/
					
					
					try {
						//use
						Log.i("[debug]", "password is " + Login.password + ", iv is " + EncryptionTools.bytesToHex(Main.iv_iv.getIV()) + ", salt is " + EncryptionTools.bytesToHex(Main.salt));
						//secretKey = EncryptionTools.generatePBEKey(Login.password.toCharArray(), salt);
						date_b64  = Base64.encode(date.getBytes(), 0);
						email_b64 = Base64.encode(email.getBytes(), 0);
						notes_b64 = Base64.encode(notes.getBytes(), 0);
						name_b64  = Base64.encode(name.getBytes(), 0);
						
						Log.i("[debug]", EncryptionTools.bytesToHex(date_b64) + " " + EncryptionTools.bytesToHex(email_b64) + " " + EncryptionTools.bytesToHex(notes_b64) + " " +  EncryptionTools.bytesToHex(name_b64));
						
						Log.i("[debug]", "SecretKey is " + EncryptionTools.bytesToHex(Main.secretKey.getEncoded()));
						
						date_ciphered  = EncryptionTools.encrpytWithPBE(date_b64,  Login.password, Main.iv_iv, Main.salt, Main.secretKey);
						email_ciphered = EncryptionTools.encrpytWithPBE(email_b64, Login.password, Main.iv_iv, Main.salt, Main.secretKey);
						name_ciphered  = EncryptionTools.encrpytWithPBE(name_b64,  Login.password, Main.iv_iv, Main.salt, Main.secretKey);
						notes_ciphered = EncryptionTools.encrpytWithPBE(notes_b64, Login.password, Main.iv_iv, Main.salt, Main.secretKey);
						
					} catch (GeneralSecurityException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					//Log.i("[debug]", "accepted record, inserting...");

					ContentValues values = new ContentValues();
					values.put(ContactsPersistence.CONTACTS_NAME,  name_ciphered);
					values.put(ContactsPersistence.CONTACTS_DATE,  date_ciphered);
					values.put(ContactsPersistence.CONTACTS_EMAIL, email_ciphered);
					values.put(ContactsPersistence.CONTACTS_NOTES, notes_ciphered);
					
					Log.d("[debug]", "Fields are: " + EncryptionTools.bytesToHex(name_ciphered)  + " " 
													+ EncryptionTools.bytesToHex(date_ciphered)  + " " 
													+ EncryptionTools.bytesToHex(email_ciphered) + " " 
													+ EncryptionTools.bytesToHex(notes_ciphered));

					
					/*
					 * End
					 */
					
					
					long id = database.insert(ContactsPersistence.CONTACTS_TABLE, null, values);
					if(id != -1){
						Log.d("[debug]", "Contact created " + id);
					}
					/* Other, less safe way:
					 * String query="INSERT INTO contacts(name, birthday, email, notes) VALUES ('"+name+"', '"+date+"', '"+email+"', '"+notes+"');";
					 * database.execSQL(query);
					 * 
					 */
					//database.close();
					Toast.makeText(AddBirthday.this, "Birthday successfully added", Toast.LENGTH_SHORT).show();
					if (nameField != null) nameField.setText("");
					if (dateField != null) dateField.setText("");
					if (emailField != null) emailField.setText("");
					if (notesField != null) notesField.setText("");
					nameField.requestFocus();

				}else if(!accepted_date && !empty_date){
					dateField.setError(getString(R.string.required_field));
					dateField.setError("Introduce date in the correct format [dd-mm-yyyy]");
					if (dateField != null) dateField.setText("");
					dateField.requestFocus();
					//Toast.makeText(AddBirthday.this, "Introduce date in the correct format [dd-mm-yyyy]", Toast.LENGTH_SHORT).show();
					if(!accepted_email && !empty_mail){
						//Toast.makeText(AddBirthday.this, "Introduce email in the correct format [aaaa@bbbb.c]", Toast.LENGTH_SHORT).show();
						emailField.setError("Introduce email in the correct format [a@b.c]");
						if (emailField != null) emailField.setText("");
					}
				}else if(!accepted_email && !empty_mail){
					Toast.makeText(AddBirthday.this, "Introduce email in the correct format [aaaa@bbbb.c]", Toast.LENGTH_SHORT).show();
					if (emailField != null) emailField.setText("");
					emailField.requestFocus();
				}
				

				
				//Necessary?
				accepted_email = false;
				accepted_date = false;
				total = 0;

			}
		});
	}
}
