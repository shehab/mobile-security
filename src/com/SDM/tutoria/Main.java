package com.SDM.tutoria;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;


public class Main extends Activity {
	
	
	Button add, view; 
	
	ContactsPersistence helper;
	SQLiteDatabase database;
	
	Cursor result_set;
	
	public static byte[] iv;
	public static byte[] salt;
	public static SecretKey secretKey;

	
	public static IvParameterSpec iv_iv; 

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		helper = new ContactsPersistence(getApplicationContext(), "contactsDB", null, 1);
		final SQLiteDatabase database=helper.getReadableDatabase();

		String [] columns = new String [2];
		columns[0] = "iv";
		columns[1] = "salt";										
		
		result_set = database.query("login", columns, null, null, null, null, null);
		//result_set = database.
		
		if(result_set != null) {
			result_set.moveToFirst();
			//iv   = result_set.getString(0).getBytes();
			iv   = result_set.getBlob(0);
			salt =  result_set.getBlob(1);
			
			iv_iv = new IvParameterSpec(iv);
			
			try {
				secretKey = EncryptionTools.generatePBEKey(Login.password.toCharArray(), salt);
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvalidKeySpecException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		
		boolean toast = false;
		
		add = (Button)findViewById(R.id.add);
		add.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Main.this, AddBirthday.class);
				startActivity(intent);
			}
		});
		
		view = (Button)findViewById(R.id.view);
		view.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String [] columns = new String [4];
				columns[0] = "name";
				columns[1] = "birthday";
				//Future development: Use these values
				columns[2] = "email";
				columns[3] = "notes";
				final Cursor result_set = database.query("contacts", columns, null, null, null, null, null);
				
				if(result_set.getCount() > 0){				
					Intent intent = new Intent(Main.this, ShowBirthday.class);
					startActivity(intent);
				} else{
					Toast.makeText(Main.this, "You do not have any reminders", Toast.LENGTH_LONG).show();
				}
			}
		});
	}
}
