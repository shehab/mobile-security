package com.SDM.tutoria;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.security.GeneralSecurityException;

import com.SDM.tutoria.SongService;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Base64.*;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class ShowBirthday extends Activity {
	int a = 0;	
	ContactsPersistence helper;
	SQLiteDatabase database;

	Button stop, home, add;
	TextView display_names, display_dates;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_show_birthday);

		startService(new Intent(getBaseContext(), SongService.class));

		getContacts();

		add = (Button)findViewById(R.id.sgoto_add);
		add.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(ShowBirthday.this, AddBirthday.class);
				startActivity(intent);
			}
		});

		home = (Button)findViewById(R.id.pgoto_home);
		home.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(ShowBirthday.this, Main.class);
				startActivity(intent);
			}
		});

		stop = (Button)findViewById(R.id.stop_music);
		stop.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Log.i("Service", "Stop service button pressed");
				stopService(new Intent(getBaseContext(), SongService.class));
			}
		});
	}

	private void getContacts(){
		helper = new ContactsPersistence(getApplicationContext(), "contactsDB", null, 1);
		SQLiteDatabase database=helper.getReadableDatabase();
		//		display_names = (TextView)findViewById(R.id.show_names);//show_dates
		//		display_dates = (TextView)findViewById(R.id.show_dates);//show_dates
		//		contacts(name, birthday, email, notes)
		String [] columns = new String [5];
		columns[0] = "id";
		columns[1] = "name";
		columns[2] = "birthday";
		//Future development: Use these values
		columns[3] = "email";
		columns[4] = "notes";
		final Cursor result_set = database.query("contacts", columns, null, null, null, null, null);

		String[] rows = new String[result_set.getCount()];
		int i = 0;
		if(result_set != null){
			if(result_set.getCount() >0){
				result_set.moveToFirst();

				do{
					byte[]a = result_set.getBlob(1);
					byte[]b = result_set.getBlob(2);



					//Log.i("[debug]", "result[0] = " + a + ", result[1] = " + b);
					Log.i("[debug]", "SecretKey is " + EncryptionTools.bytesToHex(Main.secretKey.getEncoded()));
					//write them in the view
					//display.setText(display.getText()+"\n"+result_set.getString(0)+" \t"+result_set.getString(1));
					try {
						Log.i("[debug]", "password is " + Login.password + ", iv is " + EncryptionTools.bytesToHex(Main.iv_iv.getIV()) + ", salt is " + EncryptionTools.bytesToHex(Main.salt));

						byte[] aa = EncryptionTools.decrpytWithPBE(a, Login.password, Main.iv_iv , Main.salt, Main.secretKey);
						byte[] bb = EncryptionTools.decrpytWithPBE(b, Login.password, Main.iv_iv , Main.salt, Main.secretKey);

						Log.i("[debug]", EncryptionTools.bytesToHex(Base64.decode(aa, 0)) + " " + EncryptionTools.bytesToHex(Base64.decode(bb, 0)));


						String aaa = new String(Base64.decode(aa, 0));
						String bbb = new String(Base64.decode(bb, 0));

						rows[i] = aaa + "\n" + bbb;

						//						rows[i] = EncryptionTools.decrpytWithPBE(a, Login.password, AddBirthday.iv_iv , AddBirthday.salt, AddBirthday.secretKey) 
						//								+ "\n" 
						//								+ EncryptionTools.decrpytWithPBE(b, Login.password, AddBirthday.iv_iv , AddBirthday.salt, AddBirthday.secretKey);
						i++;
					} catch (GeneralSecurityException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					Log.i("[debug]", EncryptionTools.bytesToHex(a)+" \t"+EncryptionTools.bytesToHex(b));				
				}while(result_set.moveToNext());
			}else{
				Log.i("[debug]", "No contacts");
				//				ListView lista = (ListView)findViewById(R.id.BirthdayList);
				String[] no_contacts = new String [1];
				no_contacts[0] = "You have no contacts";

				//				ArrayAdapter<String> adapter_b = new ArrayAdapter<String>(this, R.layout.list_entries, no_contacts);
				//				lista.setAdapter(adapter_b);
				//				
				ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.list_entries, new String []{"You have no contacts"}); 
				ListView list = (ListView)findViewById(R.id.BirthdayList);
				list.setAdapter(adapter);
				Toast.makeText(getApplicationContext(), "You have no added contacts!", Toast.LENGTH_LONG).show();

			}
		}
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.list_entries, rows); 
		ListView list = (ListView)findViewById(R.id.BirthdayList);
		list.setAdapter(adapter);
		list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				result_set.moveToPosition(position);
				String contact_id = Integer.toString(result_set.getInt(0));
				Intent intent = new Intent(ShowBirthday.this, ShowProfile.class);
				
				intent.putExtra("contact_id", contact_id);
				startActivity(intent);

			}
		});
		//closing resources
		//database.close();

	}



}
