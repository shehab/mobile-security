package com.SDM.tutoria;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.SDM.tutoria.ContactsPersistence;


public class Login extends Activity {
	
	ContactsPersistence helper;
	SQLiteDatabase database;
	Button loginButton;
	EditText nameField;
	
	private String pSHAHash, uSHAHash;
	
	public static String password;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
		helper = new ContactsPersistence(getApplicationContext(), "contactsDB", null, 1);
		final SQLiteDatabase database = helper.getReadableDatabase();
		
		loginButton = (Button)findViewById(R.id.login_button);
		nameField = (EditText) findViewById(R.id.l_name);
		nameField.requestFocus();	
		
		loginButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				boolean empty_username     = true;
				boolean empty_password     = true;
				
				
				String name = nameField.getText().toString();
				Log.i("[debug]", "the content of name is_" + name + "_ __");
				if(TextUtils.isEmpty(name)){
					Log.w("[debug]", "username is empty");
					nameField.setError(getString(R.string.required_field));
				}else {
					empty_username = false;
				}
				
				final EditText passField = (EditText) findViewById(R.id.l_passwd);
				String passwd = passField.getText().toString();
				if(TextUtils.isEmpty(passwd)){
					Log.w("[debug]", "passwd is empty");
					passField.setError(getString(R.string.required_field));
				}else{
					
					empty_password = false;
					password = passwd;
				}
				
				passwd += R.string.salty_salt;

				System.out.println(" vamos a ver que pasa aqui: " +  passwd);
				MessageDigest mdSha1 = null;
				try
				{
					mdSha1 = MessageDigest.getInstance("SHA-256");
					mdSha1.update(passwd.toString().getBytes()); // obtenemos la secuencias de bytes
				} catch (NoSuchAlgorithmException e1) {
					Log.e("miApp", "Error al inicializar el SHA1 message digest");
				}
				byte[] pdata = mdSha1.digest(); // pasamos los digitos a un arreglo de bytes
				
				try {
					pSHAHash = convertToHex(pdata);
				} catch (IOException e2) {
					e2.printStackTrace();
				}
				System.out.println(" vamos a ver que pasa y aqui tambien: " +  pSHAHash);
				
				mdSha1 = null;
				System.out.println(" vamos a ver que pasa aqui: " +  name);
				try
				{
					mdSha1 = MessageDigest.getInstance("SHA-256");
					mdSha1.update(name.toString().getBytes()); // obtenemos la secuencias de bytes
				} catch (NoSuchAlgorithmException e1) {
					Log.e("miApp", "Error al inicializar el SHA1 message digest");
				}
				byte[] udata = mdSha1.digest(); // pasamos los digitos a un arreglo de bytes
				
				try {
					uSHAHash = convertToHex(udata);
				} catch (IOException e) {
					e.printStackTrace();
				}
				System.out.println(" vamos a ver que pasa y aqui tambien: " +  uSHAHash);				
				
				
					
				

				if(!empty_username && !empty_password){
					Log.i("[debug]", "accepted credentials, checking...");
		
					String [] columns = new String [2];
					columns[0] = "username";
					columns[1] = "password";
					
					final Cursor result_set = database.query("login", columns, null, null, null, null, null);
					String d_uname, d_pass;
					if(result_set != null){
						if(result_set.getCount() >0){
							result_set.moveToFirst();
							d_uname = result_set.getString(0);
							d_pass  = result_set.getString(1);
							if(uSHAHash.compareTo(d_uname) == 0 && pSHAHash.compareTo(d_pass) == 0){
								Toast.makeText(Login.this, "Credentials successfully introduced", Toast.LENGTH_SHORT).show();

								Intent intent = new Intent(Login.this, Main.class);
								startActivity(intent);
							} else {
								Toast.makeText(Login.this, "Unable to validate credentials", Toast.LENGTH_SHORT).show();
								if (nameField != null) nameField.setText("");
								if (passField != null) passField.setText("");
								nameField.requestFocus();
							}
						}else {
							Toast.makeText(Login.this, "Unable to validate credentials", Toast.LENGTH_SHORT).show();
							if (nameField != null) nameField.setText("");
							if (passField != null) passField.setText("");
							nameField.requestFocus();
						}
					}else {
						Toast.makeText(Login.this, "Unable to validate credentials", Toast.LENGTH_SHORT).show();
						if (nameField != null) nameField.setText("");
						if (passField != null) passField.setText("");
						nameField.requestFocus();
					}										
//					Intent intent = new Intent(Login.this, Main.class);
//					startActivity(intent);					
					//Toast.makeText(Login.this, "Credentials successfully introduced", Toast.LENGTH_SHORT).show();
				
				}	
			}
		});
	}
	
	private static String convertToHex(byte[] data) throws java.io.IOException
	{         
		StringBuffer sb = new StringBuffer();
		for(byte recogerDatos : data)
		{
			sb.append(Integer.toString((recogerDatos & 0xff) + 0x100, 16).substring(1));
		}
		return sb.toString();
	}
}
